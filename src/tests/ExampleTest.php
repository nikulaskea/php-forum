<?php 
    use PHPUnit\Framework\TestCase;

    class Example extends TestCase {
        public function testExampleFunction() {
            require_once('test.php');
            $expected = 'example test';
            $result = exampleFunction();
            $this->assertEquals($expected, $result);

        }
    };
?>