<?php
    // DB params
    define('DB_HOST', 'db');
    define('DB_USER', 'root');
    define('DB_PASS', 'example');
    DEFINE('DB_NAME', 'forum');
    // App root
    define('APPROOT', dirname(dirname(__FILE__)));
    // URL root
    define('URLROOT', 'http://localhost');
    // Public root
    define('ASSETS', dirname(dirname(dirname(__FILE__))) . '/public/assets');
    // Site name
    define('SITENAME', 'Forum');